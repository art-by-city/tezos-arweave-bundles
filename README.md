# Art By City - Tezos Arbundles

This project is a wrapper library written in TypeScript to add Tezos support to [`arbundles`](https://github.com/Bundlr-Network/arbundles).

## Install
```bash
$ npm i --save @tezos-arbundles
```

## Usage
Create a `TezosSigner` & sign a `DataItem`
```typescript
import { bundleAndSignData, createData, TezosSigner } from 'tezos-arbundles'

// Create an ArBundles signer using Tezos secret key
const signer = new TezosSigner('<tezos secret key>')
await signer.setPublicKey()

// Create & sign a DataItem
const data: string | Uint8Array = 'Signed by a tezos wallet!'
const dataItem = createData(data, signer)
await dataItem.sign(signer)

// Add DataItem to a Bundle
const dataItems = [ dataItem ]
const bundle = await bundleAndSignData(dataItems, signer)

// POST to tezos-bundler
myFavoritePostMethod('/bundle/xtz', bundle.getRaw())
```

