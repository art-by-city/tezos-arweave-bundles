import 'mocha'
import { expect } from 'chai'
import { createData } from 'arbundles'

import TezosSigner from '../../src/tezos-signer'

describe('Tezos Signer', () => {
  it('signs and verifies data items', async () => {
    const key = 'edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq'
    const pk = 'edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn'
    const signer = new TezosSigner(key)
    await signer.setPublicKey()
    const dataItem = createData('test string', signer)

    await dataItem.sign(signer)

    expect(dataItem.isSigned()).to.be.true
    expect(await dataItem.isValid()).to.be.true
    expect(
      await TezosSigner.verify(
        pk,
        await dataItem.getSignatureData(),
        dataItem.rawSignature
      )
    ).to.be.true
  })
})
