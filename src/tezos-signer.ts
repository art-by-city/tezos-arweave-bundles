import * as ed25519 from '@noble/ed25519'
import { b58cdecode, prefix } from '@taquito/utils'
import { Signer } from 'arbundles/src/signing'
import { SIG_CONFIG, SignatureConfig } from 'arbundles/src/constants'

export default class TezosSigner implements Signer {
  signatureType: number = SignatureConfig.ED25519
  signatureLength: number = SIG_CONFIG[SignatureConfig.ED25519].sigLength
  ownerLength: number = SIG_CONFIG[SignatureConfig.ED25519].pubLength
  private _key!: Uint8Array
  private _pk!: Uint8Array

  get publicKey(): Buffer {
    return Buffer.from(this._pk)
  }

  constructor(sk: string) {
    const keyPrefix = sk.substr(0, 4)
    this._key = b58cdecode(
      sk,
      // @ts-ignore
      prefix[keyPrefix]
    )
  }

  async setPublicKey() {
    this._pk = await ed25519.getPublicKey(this._key)
  }

  async sign(message: Uint8Array): Promise<Uint8Array> {
    return await ed25519.sign(message, this._key)
  }

  static async verify(
    pk: string | Buffer,
    message: Uint8Array,
    signature: Uint8Array
  ): Promise<boolean> {
    const pkString = pk.toString()
    const keyPrefix = pkString.substr(0, 4)
    const _pk = b58cdecode(
      pkString,
      // @ts-ignore
      prefix[keyPrefix]
    )
    return await ed25519.verify(signature, message, _pk)
  }
}
