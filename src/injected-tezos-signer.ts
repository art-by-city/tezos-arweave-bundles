import { Signer } from 'arbundles/src/signing'
import { SIG_CONFIG, SignatureConfig } from 'arbundles/src/constants'
import { BeaconWallet } from '@taquito/beacon-wallet'
import { SigningType } from '@airgap/beacon-sdk'
import { b58cdecode, b58cencode, char2Bytes, prefix } from '@taquito/utils'
import { Prefix } from '@taquito/utils'

/**
 * WIP: EXPERIMENTAL!
 */
export default class InjectedTezosSigner implements Signer {
  signatureType: number = SignatureConfig.ED25519
  signatureLength: number = SIG_CONFIG[SignatureConfig.ED25519].sigLength
  ownerLength: number = SIG_CONFIG[SignatureConfig.ED25519].pubLength
  private _wallet!: BeaconWallet
  private _pk!: Uint8Array

  get publicKey(): Buffer {
    return Buffer.from(this._pk)
  }

  constructor(wallet: BeaconWallet) {
    this._wallet = wallet
  }

  async setPublicKey() {
    const accountInfo = await this._wallet.client.getActiveAccount()

    if (accountInfo) {
      const pkString = accountInfo.publicKey.toString()
      // console.log('PKSTRING', pkString)
      const keyPrefix = pkString.substr(0, 4)
      this._pk = b58cdecode(
        pkString,
        // @ts-ignore
        prefix[keyPrefix]
      )
      // console.log('PKLENGTH', this._pk.length, this._pk.toString())
      // console.log('PKSTRING ENCODED', b58cencode(
      //   this._pk,
      //   // @ts-ignore
      //   prefix[keyPrefix]
      // ))
    } else {
      throw new Error('Could not get public key from Beacon Wallet')
    }
  }

  async sign(message: Uint8Array): Promise<Uint8Array> {
    const messageString = [
      'Tezos Signed Message:',
      'localhost',
      new Date().toISOString(),
      new TextDecoder().decode(message)
    ].join(' ')
    const messageBytes = char2Bytes(messageString)
    const payload = '05'
      + '0100'
      + char2Bytes(messageBytes.length.toString())
      + messageString

    const sourceAddress = await this._wallet.getPKH()

    // console.log('GOT SOURCE ADDRESS', sourceAddress)

    // const response = await this._wallet.client.requestSignPayload({
    //   signingType: SigningType.MICHELINE,
    //   sourceAddress,
    //   payload
    // })

    const response = await this._wallet.client.requestSignPayload({
      signingType: SigningType.RAW,
      payload: new TextDecoder().decode(message)
    })

    // console.log('GOT SIGNATURE', response.signature.substring(4))
    // const sigdecoded =
    // console.log('GOT SIGNATURE DECODED', sigdecoded)
    // const textEncoded = new TextEncoder().encode(response.signature.substring(4))
    // console.log('GOT TEXTENCODED', textEncoded)

    return b58cdecode(response.signature, prefix[Prefix.EDSIG])
  }
}
