import { Bundle, DataItem } from 'arbundles'
import { Signer } from 'arbundles/src/signing'
import { longTo32ByteArray } from 'arbundles/src/utils'

export { default as TezosSigner } from './tezos-signer'
export { default as InjectedTezosSigner } from './injected-tezos-signer'

/**
 * Create a Bundle from signed DataItem
 *
 * @param items
 * @returns Bundle
 */
export async function createBundleFromSignedDataItems(
  items: DataItem[]
): Promise<Bundle> {
  const headers = Buffer.alloc(64 * items.length)
  const binaries = items.map((item, index) => {
    const header = Buffer.alloc(64)
    header.set(longTo32ByteArray(item.getRaw().byteLength), 0)
    header.set(item.rawId, 32)

    headers.set(header, 64 * index)

    return item.getRaw()
  })

  const buffer = Buffer.concat([
    longTo32ByteArray(items.length),
    headers,
    ...binaries
  ])

  return new Bundle(buffer)
}
